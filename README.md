## Installation

```shell
python -m pip install -r requirements.txt
```

## Run the backend

Start the application:

```
python backend.py
```

Test it easily with the swagger-ui at: [http://127.0.0.1:5000/docs](http://127.0.0.1:5000/docs).