from __future__ import annotations
from typing import List, Optional
from pydantic import BaseModel, Field
from uuid import UUID, uuid4


class Connection(BaseModel):
    name: str


class Device(BaseModel):
    name: str


class Background(BaseModel):
    name: str


class Layer(BaseModel):
    devices: List[Device] = []
    connections: List[Connection] = []
    background: Optional[Background] = None


class ModelBase(BaseModel):
    name: str
    layers: List[Layer] = []


class ModelRequest(ModelBase):
    pass


class ModelResponse(ModelBase):
    id: UUID = Field(default_factory=uuid4)
    tenant: str

    @staticmethod
    def from_base_model(tenant: str, model: ModelBase) -> ModelResponse:
        return ModelResponse(tenant=tenant, **model.dict())
