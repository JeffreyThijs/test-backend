import uvicorn
from typing import Dict, List
from fastapi import FastAPI, Path
from uuid import UUID

from models import Background, Layer, ModelRequest, ModelResponse

app = FastAPI()

first_entry = ModelResponse(
    name="My first model!",
    tenant="aloxy",
    layer=[Layer(background=Background(name="some-background"))]
)

in_memory_database: Dict[UUID, ModelResponse] = {
    first_entry.id: first_entry
}


@app.post("/models/tenant/{tenantName}")
async def post_model(model: ModelRequest, tenant_name: str = Path(None, alias="tenantName")) -> UUID:
    model_out = ModelResponse.from_base_model(tenant_name, model)
    in_memory_database[model_out.id] = model_out
    return model_out.id


@app.get("/models/tenant/{tenantName}")
async def get_models_by_tenant(tenant_name: str = Path(None, alias="tenantName")) -> List[ModelResponse]:
    return list(filter(lambda x: x.tenant == tenant_name, in_memory_database.values()))


@app.get("/models")
async def get_models() -> List[ModelResponse]:
    return list(in_memory_database.values())

if __name__ == "__main__":
    uvicorn.run("backend:app", host="127.0.0.1", port=5000, log_level="debug", reload=True)
